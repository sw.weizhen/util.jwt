package main

import (
	"fmt"

	jwt "gitlab.com/sw.weizhen/util.jwt"
)

type Token struct {
	Uid  int
	Name string
	jwt.RegisteredClaimNames
}

func main() {
	fmt.Println("sw.jwt::main()")

	// tk := Token{
	// 	Uid:  1,
	// 	Name: "S.W.",
	// 	RegisteredClaimNames: jwt.RegisteredClaimNames{
	// 		ExpirationTime: time.Now().Add(time.Second * 5).Unix(),
	// 	IssuedAt: time.Now().Add(time.Second * 3).Unix(),
	// 	},
	// }

	// mySigningKey := []byte(`Agill(*+@15)=?x`)

	// token := jwt.New(jwt.SigningHS256, tk)
	// sign, err := token.Sign(mySigningKey)

	// if err != nil {
	// 	fmt.Printf("err: %v\n", err)
	// 	return
	// }
	// fmt.Printf("%v\n\n", sign)

	// time.Sleep(time.Second * 6)

	// jwtk, err := jwt.Decrypt(sign, func(token *jwt.JWToken) (interface{}, error) {
	// 	return []byte("Agill(*+@15)=?x"), nil
	// })

	// if err != nil {
	// 	fmt.Printf("jwt decrypt err: %v\n", err)
	// }

	// if jwtk.Valid {
	// 	fmt.Printf("%+v\n", jwtk.Claims)
	// }

	token := `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVaWQiOjEsIk5hbWUiOiJTLlcuIiwianRpIjoiIiwic3ViIjoiIiwiYXVkIjoiIiwiaXNzIjoiIiwiaWF0IjowLCJuYmYiOjAsImV4cCI6MH0.2npV61LZ0twMnKgiyL6lVhbbelvVFWeBkBYSoUs_UgU`
	jwt, err := jwt.Decrypt(token, func(token *jwt.JWToken) (interface{}, error) {
		return []byte("Agill(*+@15)=?x"), nil
	})

	if err != nil {
		fmt.Printf("err: %v\n", err)
		return
	}

	fmt.Printf("Valid: %v\n", jwt.Valid)
	fmt.Printf("SignatureOpe: %+v\n", jwt.SignatureOpe)
	fmt.Printf("Header: %v\n", jwt.Header)
	fmt.Printf("Claims: %v\n", jwt.Claims)
	fmt.Printf("Signature: %v\n", jwt.Signature)

}
