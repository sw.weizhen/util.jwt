package gojwt

import (
	"encoding/json"
	"errors"
)

type MapClaims map[string]interface{}

func (ref MapClaims) verifyExpiresAt(now int64, required bool) bool {
	switch exp := ref[TAG_EXP].(type) {
	case float64:
		return expired(int64(exp), now, required)

	case json.Number:
		val, _ := exp.Int64()
		return expired(val, now, required)
	}

	return !required
}

func (ref MapClaims) verifyIssuedAt(now int64, required bool) bool {
	switch iat := ref[TAG_IAT].(type) {
	case float64:
		return issuedAt(int64(iat), now, required)

	case json.Number:
		val, _ := iat.Int64()
		return issuedAt(val, now, required)
	}

	return !required
}

func (ref MapClaims) VerifyNotBefore(now int64, required bool) bool {
	switch nbf := ref[TAG_NBF].(type) {
	case float64:
		return notBefore(int64(nbf), now, required)

	case json.Number:
		val, _ := nbf.Int64()
		return notBefore(val, now, required)
	}

	return !required
}

func (ref MapClaims) Validating() error {
	err := new(ErrorValidation)
	now := TimeNow().Unix()

	if !ref.verifyExpiresAt(now, false) {
		err.ErrStores = errors.New(ERR_TOKEN_EXPIRED)
		err.Errors |= ERR_VALIDATION_EXPIRED
	}

	if !ref.verifyIssuedAt(now, false) {
		err.ErrStores = errors.New(ERR_TOKEN_USED_BEFORE_ISSUED)
		err.Errors |= ERR_VALIDATION_ISSUED_AT
	}

	if !ref.VerifyNotBefore(now, false) {
		err.ErrStores = errors.New(ERR_TOKEN_NOT_VALID_YET)
		err.Errors |= ERR_VALIDATION_NOT_VALID_YET
	}

	if err.isValid() {
		return nil
	}

	return err
}
